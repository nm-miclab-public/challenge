

import os
import glob
from PIL import Image

from tqdm import tqdm as tqdm
import cv2
import numpy as np
import nibabel as nib
import pandas as pd
import matplotlib.pyplot as plt
import torchvision as tv
import torch


def axial_func(im):
    im = im.transpose(1, 2, 0).mean(0).astype(np.float32)
    im = tv.transforms.ToPILImage()(im)
    return im


def sagittal_func(im):
    im = im.transpose(0, 2, 1).mean(0).astype(np.float32)
    im = tv.transforms.ToPILImage()(im)
    im = tv.transforms.functional.rotate(im, 180, expand=True)
    return im


def coronal_func(im):
    im = im.transpose(1, 0, 2).mean(0).astype(np.float32)
    im = tv.transforms.ToPILImage()(im)
    im = tv.transforms.functional.rotate(im, 90, expand=True)
    return im


def main(root, output, csv_path, view):

    if not os.path.exists(output):
        os.mkdir(output)

    for n in ['COVID', 'Normal', 'Opacidade']:
        if not os.path.exists(os.path.join(output, n)):
            os.mkdir(os.path.join(output, n))

    if view == 'axial':
        view_func = axial_func
    elif view == 'coronal':
        view_func = coronal_func
    elif view == 'sagittal':
        view_func = sagittal_func

    COVID = 1
    NON = 0

    MAX = 600
    MIN = -1024

    # df with labels
    df = pd.read_csv(csv_path)
    df = df[df['ruim'] != '*']

    for row in tqdm(df.iloc, total=len(df)):
        _id = row['id']
        label = row['Diagnostico']

        path = os.path.join(output, label, f'{_id}.pth')

        if os.path.exists(path):
            continue

        im = nib.load(os.path.join(root, f'{_id}.nii'))
        im = im.get_fdata()

        im = np.clip(im, MIN, MAX)
        im = (im - MIN) / (MAX - MIN)

        im = view_func(im)
        im = tv.transforms.ToTensor()(im)[0]
        torch.save(im, path)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Preproccess CT images.')

    parser.add_argument(
        '--root', type=str, help='Root folder containing all nii images to be proccessed')
    parser.add_argument(
        '--output', type=str, help='Output folder where the proccessed tensors will be saved')
    parser.add_argument(
        '--csv_path', type=str, help='CSV containing the id and label for each image')
    parser.add_argument(
        '--view', type=str, help='View to be proccessed. Must be one of axial, coronal or sagittal')

    args = parser.parse_args()

    main(root=args.root, output=args.output,
         csv_path=args.csv_path, view=args.view)
