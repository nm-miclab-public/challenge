'''
Offline component post-processing
'''
import argparse
import os
import glob
import multiprocessing as mp

import numpy as np
import nibabel as nib
from tqdm import tqdm

from DLPT.post.labeling3d import get_connected_components


def worker(path):
    data = nib.load(path)
    affine = data.affine

    output = data.get_fdata()
    output, _, _ = get_connected_components(output.astype(np.int32)).astype(np.int16)

    nib.save(nib.Nifti1Image(output, affine=affine), os.path.join(os.path.dirname(path), "post", os.path.basename(path)))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', type=str, required=True,
                        help=("Input folder."))
    parser.add_argument('-c', '--cpus', type=int, default=mp.cpu_count(),
                        help="Number of cores used. A higher number consumes a lot more RAM. Defaults to number of cores available.")
    args = parser.parse_args()

    pool = mp.Pool(args.cpus)

    paths = glob.glob(os.path.join(args.input, "*.nii*"))

    os.makedirs(os.path.join(args.input, "post"), exist_ok=True)

    print(f"Input: {paths}")

    for _ in tqdm(pool.imap_unordered(worker, paths), total=len(paths)):
        pass
