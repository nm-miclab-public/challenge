'''
Assigns the correct transforms according to experiment description
'''
import numpy as np
from scipy.ndimage import zoom

from DLPT.transforms import Compose
from DLPT.transforms.patches import ReturnPatch, CenterCrop
from DLPT.transforms.to_tensor import ToTensor
from DLPT.transforms.intensity import RandomIntensity


class FixWrongTargets():
    def __call__(self, ct, mask):
        if mask is not None:
            isclose_mask = np.isclose(mask, 3)
            if isclose_mask.sum() > 0:
                mask[isclose_mask] = 1

        return ct, mask

    def __str__(self):
        return "FixWrongTargets"


class CovidTargetProcessing():
    def __init__(self, lonly=False):
        self.lonly = lonly

    def __call__(self, ct, mask):
        if mask is not None:
            if np.isclose(mask, 3).sum() > 0:
                raise ValueError("Incorrect mask supplied, label 3 detected.")
            if self.lonly:
                target = np.zeros((1,) + mask.shape, dtype=np.float32)
                target[0][np.isclose(mask, 2)] = 1.0  # Lesões
            else:
                target = np.zeros((2,) + mask.shape, dtype=np.float32)
                target[0][np.isclose(mask, 1)] = 1.0
                target[1][np.isclose(mask, 2)] = 1.0  # Lesões
                target[0] = target[0] + target[1]  # Pulmão
        else:
            target = mask

        return ct, target

    def __str__(self):
        return f"CovidTargetProcessing lonly: {self.lonly}"


class NormalizeCT():
    '''
    Clips on min max values and min max normalizes [0, 1].
    '''
    WARNED = False

    def __init__(self, minv=None, maxv=None):
        self.min = minv
        self.max = maxv

    def __call__(self, ct, mask):
        if self.min is not None and self.max is not None:
            ct = np.clip(ct, self.min, self.max)
        elif not NormalizeCT.WARNED:
            print("WARNING: Not performing clipping.")
            NormalizeCT.WARNED = True

        ct = (ct - ct.min())/(ct.max() - ct.min())
        return ct, mask

    def __str__(self):
        return f"NormalizeCT min: {self.min} max: {self.max}"


class Resize():
    def __init__(self, target_shape):
        self.target_shape = np.array(target_shape)

    def __call__(self, ct, mask):
        original_shape = np.array(ct.shape)

        factor = self.target_shape/original_shape

        if mask is None:
            return zoom(ct, factor), mask
        else:
            return zoom(ct, factor), zoom(mask, factor, order=0)

    def __str__(self):
        return f"Resize to {self.target_shape}"


def desc2transform(desc):
    if "baseline" in desc:
        train = Compose([NormalizeCT(minv=None, maxv=None),
                         FixWrongTargets(),
                         ReturnPatch(ppositive=None, patch_size=(256, 256, 128), fullrandom=True),
                         CovidTargetProcessing(),
                         ToTensor(volumetric=True)])
        test = Compose([NormalizeCT(minv=None, maxv=None),
                        FixWrongTargets(),
                        Resize((256, 256, 128)),
                        CovidTargetProcessing(),
                        ToTensor(volumetric=True)])
    elif "norm_pp_ndata" in desc or "slower_upsample" in desc or "backbone" in desc:
        train = Compose([NormalizeCT(minv=-1024, maxv=600),
                         FixWrongTargets(),
                         ReturnPatch(ppositive=1, patch_size=(256, 256, 128)),
                         CovidTargetProcessing(),
                         ToTensor(volumetric=True)])
        test = Compose([NormalizeCT(minv=-1024, maxv=600),
                        FixWrongTargets(),
                        Resize((256, 256, 128)),
                        CovidTargetProcessing(),
                        ToTensor(volumetric=True)])
    elif "small_post_challenge" in desc:
        train = Compose([NormalizeCT(minv=-1024, maxv=600),
                         FixWrongTargets(),
                         ReturnPatch(ppositive=1, patch_size=(256, 256, 128)),
                         CovidTargetProcessing(),
                         ToTensor(volumetric=True)])
        test = Compose([NormalizeCT(minv=-1024, maxv=600),
                        FixWrongTargets(),
                        CenterCrop(448, 448, 128),
                        CovidTargetProcessing(),
                        ToTensor(volumetric=True)])
    elif "small_unet" in desc:
        # This is "small_post_challenge" with traditional validation transforms
        train = Compose([NormalizeCT(minv=-1024, maxv=600),
                         FixWrongTargets(),
                         ReturnPatch(ppositive=1, patch_size=(256, 256, 128)),
                         CovidTargetProcessing(),
                         ToTensor(volumetric=True)])
        test = Compose([NormalizeCT(minv=-1024, maxv=600),
                        FixWrongTargets(),
                        Resize((256, 256, 128)),
                        CovidTargetProcessing(),
                        ToTensor(volumetric=True)])
    elif "dual" in desc:
        train = Compose([NormalizeCT(minv=-1024, maxv=600),
                         FixWrongTargets(),
                         CovidTargetProcessing(),
                         ReturnPatch(ppositive=1, patch_size=(256, 256, 128), anyborder=1),
                         ToTensor(volumetric=True)])
        test = Compose([NormalizeCT(minv=-1024, maxv=600),
                        FixWrongTargets(),
                        Resize((256, 256, 128)),
                        CovidTargetProcessing(),
                        ToTensor(volumetric=True)])
    elif "smallerp" in desc:
        train = Compose([NormalizeCT(minv=-1024, maxv=600),
                         FixWrongTargets(),
                         CovidTargetProcessing(),
                         ReturnPatch(ppositive=1, patch_size=(128, 128, 64), anyborder=1),
                         ToTensor(volumetric=True)])
        test = Compose([NormalizeCT(minv=-1024, maxv=600),
                        FixWrongTargets(),
                        Resize((256, 256, 128)),
                        CovidTargetProcessing(),
                        ToTensor(volumetric=True)])
    elif "lonly" in desc:
        train = Compose([NormalizeCT(minv=-1024, maxv=600),
                         FixWrongTargets(),
                         CovidTargetProcessing(),
                         ReturnPatch(ppositive=1, patch_size=(128, 128, 64), anyborder=1),
                         ToTensor(volumetric=True)])
        test = Compose([NormalizeCT(minv=-1024, maxv=600),
                        FixWrongTargets(),
                        Resize((256, 256, 128)),
                        CovidTargetProcessing(),
                        ToTensor(volumetric=True)])
    elif "lonopatchp" in desc:
        train = Compose([NormalizeCT(minv=-1024, maxv=600),
                         FixWrongTargets(),
                         Resize((256, 256, 128)),
                         CovidTargetProcessing(),
                         ToTensor(volumetric=True)])
        test = Compose([NormalizeCT(minv=-1024, maxv=600),
                        FixWrongTargets(),
                        Resize((256, 256, 128)),
                        CovidTargetProcessing(),
                        ToTensor(volumetric=True)])
    elif "lo_200norm_bpatch" in desc:
        train = Compose([NormalizeCT(minv=-1024, maxv=200),
                         FixWrongTargets(),
                         CovidTargetProcessing(),
                         ReturnPatch(ppositive=1, patch_size=(256, 256, 128), anyborder=1),
                         RandomIntensity(reset_seed=False),
                         ToTensor(volumetric=True)])
        test = Compose([NormalizeCT(minv=-1024, maxv=200),
                        FixWrongTargets(),
                        Resize((256, 256, 128)),
                        CovidTargetProcessing(),
                        ToTensor(volumetric=True)])
    elif desc is None:
        train = ToTensor(volumetric=True)
        test = ToTensor(volumetric=True)
    else:
        raise ValueError(f"{desc} has no transform defined.")

    return train, test


def coedet_transform(trans):
    if trans == "baseline":
        train = Compose([ReturnPatch(ppositive=None, patch_size=(256, 256), fullrandom=True),
                         RandomIntensity(reset_seed=False),
                         ToTensor(volumetric=False)])
        test = ToTensor(volumetric=False)

    return train, test
