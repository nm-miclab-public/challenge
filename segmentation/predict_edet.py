'''
CLI Utility to run over a single CT input or folder in a batched and optimized manner.
Also re-peforms validation.
'''
import os
import traceback
import argparse
from typing import List

import numpy as np
import torch
import glob
import time
import nibabel as nib
import multiprocessing as mp
from tqdm import tqdm
from scipy.ndimage import binary_erosion

from torch.utils.data import DataLoader, Dataset
import pytorch_lightning as pl

from DLPT.metrics.segmentation import DICEMetric
from DLPT.post.labeling3d import get_connected_components

from segmentation_report import segmentation_report
from ct_dataset import CTCovid19
from experiment_edet import CoEDETExperiment
from transforms import Compose, CovidTargetProcessing, FixWrongTargets, ToTensor, NormalizeCT


def load_model_by_desc(desc, models, device="cuda:0"):
    '''
    Looks for a model with the the provided description and returns it ready for prediction.
    '''
    print(f"Loading {desc} model...")
    model_path = glob.glob(os.path.join(models, f"*{desc}*"))
    assert len(model_path) == 1, f"Multiple or no weight with description {desc}"
    model_path = model_path[0]
    print(f"Loading from: {model_path}")
    model = CoEDETExperiment.load_from_checkpoint(model_path)
    model = model.eval()
    model = model.to(device)

    return model


def prepare_output(output):
    '''
    Create folders for local debug outputs
    '''
    os.makedirs(os.path.join(output, "lung"), exist_ok=True)
    os.makedirs(os.path.join(output, "covid"), exist_ok=True)
    os.makedirs(os.path.join(output, "report"), exist_ok=True)
    os.makedirs(os.path.join(output, "summary"), exist_ok=True)
    print(f"Predictions will be saved in {output}/lung, {output}/covid, {output}/report and {output}/summary.")


def get_test_data(x):
    '''
    DataLoader like loading for external test volumes
    '''
    ct, transform = x
    if isinstance(ct, str):
        data = nib.load(ct)
        if ".nii.gz" in ct:
            ID = os.path.basename(os.path.normpath(ct))[:-7]
        elif ".nii" in ct:
            ID = os.path.basename(os.path.normpath(ct))[:-4]
        affine = data.affine
        inpt = data.get_fdata(dtype=np.float32)
        try:
            pixdim = data.header["pixdim"]
        except Exception:
            print("WARNING: Malformed header, will assume voxel volume is 1mm3.")
            pixdim = [0, 1, 1, 1]
    else:
        inpt, ID = ct
        inpt = inpt.astype(np.float32)
        affine = None
        pixdim = None

    inpt, _ = transform(inpt, None)

    return inpt.unsqueeze(0), ID, affine, pixdim


class TestDataloaderLike(object):
    def __init__(self, ct, transform):
        '''
        Simulates a traininag dataloader but for prediction.

        ct is either [(file, ID)] or [path]
        '''
        self.ct = ct
        self.transform = transform

    def __len__(self):
        return len(self.ct)

    def __getitem__(self, i):
        return get_test_data((self.ct[i], self.transform))


class SliceDataloader(Dataset):
    def __init__(self, inpt):
        '''
        Treat a volume (inpt) as a dataset of slices.
        '''
        print("Rearranging shape for segmentation")
        print(inpt.shape)
        inpt = inpt.squeeze(0)  # [B, C, H, W, Fatia] -> [C, H, W, Fatia]
        print(inpt.shape)
        inpt = inpt.permute(3, 0, 1, 2)  # [C, H, W, Fatia] -> [Fatia, C, H, W]
        print(inpt.shape)
        self.inpt = inpt

    def __len__(self):
        return self.inpt.shape[0]

    def __getitem__(self, i):
        return self.inpt[i]  # dataloader will bring this dimension back

    def get_dataloader(self, batch_size):
        return DataLoader(self, batch_size=batch_size, pin_memory=True, shuffle=False, num_workers=mp.cpu_count())


def labeling_worker(x):
    '''
    Operates over 3D labeling lung or covid.
    '''
    label, volume, return_largets = x
    return {label: get_connected_components(volume, return_largest=return_largets)}


def erode_worker(x):
    '''
    Performs erode in a process worker.
    '''
    label, volume = x
    return {label: volume - binary_erosion(volume, structure=np.ones((3, 3, 1)))}


def predict_2d_stack(model: pl.LightningModule,  # TODO nn.Module
                     dataloader: object,
                     validation: bool,
                     output: str,
                     compress: bool,
                     dice: object,
                     P_dices: List,
                     L_dices: List,
                     device: torch.device,
                     orientation_information: List,
                     batch_size: int = 5,
                     use_nift: bool = True,
                     voxvol: float = 1,
                     accession_number: str = 'NA',
                     study_date: str = 'NA'):
    '''
    Function that handles segmentation over a given dataloader, that can or can't have validation information.

    This function is currently monolithic and might need improvements in the future.
    '''
    tqdm_iter = tqdm(dataloader, desc="Segmentation processing...", position=0)
    ret = {}
    for batch in tqdm_iter:
        if validation:
            inpt, mask, meta_data = batch
            mask = mask.squeeze()
            ID = meta_data['ID'][0]
            affine = meta_data["affine"].numpy()[0]
        else:
            mask = None
            inpt, ID, affine, pixdim = batch

        outs = []

        slice_dataloader = SliceDataloader(inpt).get_dataloader(batch_size)
        with torch.no_grad():
            outs = torch.cat([model(slices.to(device)).cpu() for slices in tqdm(slice_dataloader, desc="Segmenting...", position=1)],
                             dim=0)
        cpu_pred = outs.permute(1, 2, 3, 0).numpy()  # [Fatia, C, H, W] -> [C, H, W, Fatia]
        tqdm_iter.write(f"Segmentation output shape: {cpu_pred.shape}")

        tqdm_iter.write("Post-processing...")

        l_begin = time.time()

        labeling_inputs = [("lung", (cpu_pred[0] > 0.5).astype(np.int32), 2), ("covid", (cpu_pred[1] > 0.5).astype(np.int32), 0)]

        worker_pool = mp.Pool(mp.cpu_count())
        for labeling_result in tqdm(worker_pool.imap_unordered(labeling_worker, labeling_inputs), total=len(labeling_inputs),
                                    desc="3D Labeling...", position=1):
            for label, result in labeling_result.items():
                if label == "lung":
                    lung, lung_lc, lung_labeled = result
                elif label == "covid":
                    covid, covid_lc, covid_labeled = result
        tqdm_iter.write(f"Labeling time: {round(time.time() - l_begin)}")

        tqdm_iter.write("Generating reports...")
        covid, dict_seg_report = segmentation_report(lung_lc, lung_labeled, covid_lc, covid_labeled, voxvol, tqdm_iter,
                                                     orientation_information=orientation_information)

        # Following dtypes from input masks from challenge
        covid = covid.astype(np.int16)
        lung = lung.astype(np.int16)

        lung_path = os.path.join(output, "lung", f"{ID}_mask_lung.npz")
        covid_path = os.path.join(output, "covid", f"{ID}_mask_covid.npz")
        tqdm_iter.write("Saving masks...")
        np.savez_compressed(lung_path, data=lung)
        np.savez_compressed(covid_path, data=covid)

        if mask is not None:
            # Pulmão
            P_dice = dice(torch.from_numpy(lung), mask[0])
            P_dices.append(P_dice)

            # Lesões
            L_dice = dice(torch.from_numpy(covid), mask[1])
            L_dices.append(L_dice)
            tqdm_iter.write(f"Pulmão: {P_dice}")
            tqdm_iter.write(f"Lesão: {L_dice}")

        erode_inputs = [("lung", lung), ("covid", covid)]
        for erode_results in tqdm(worker_pool.imap_unordered(erode_worker, erode_inputs), total=len(erode_inputs),
                                  desc="3D Eroding...", position=1):
            for label, result in erode_results.items():
                if label == "lung":
                    lung_borders = result
                elif label == "covid":
                    covid_borders = result
        worker_pool.close()
        worker_pool.join()

        lung_borders_path = os.path.join(output, "lung", f"{ID}_border_lung.npz")
        covid_borders_path = os.path.join(output, "covid", f"{ID}_border_covid.npz")
        tqdm_iter.write("Saving borders...")
        np.savez_compressed(lung_borders_path, data=lung_borders)
        np.savez_compressed(covid_borders_path, data=covid_borders)

        inpt = inpt.cpu().squeeze().numpy()

        projection_path = os.path.join(output, "report", f"{ID}_projection.npz")
        projections = []
        for ax in range(3):
            ct_proj = inpt.mean(axis=ax)
            lung_proj = lung.mean(axis=ax)
            covid_proj = covid.mean(axis=ax)
            projection = np.zeros((3,) + ct_proj.shape, dtype=np.float32)
            projection[0] = ct_proj
            projection[1] = lung_proj
            projection[2] = covid_proj
            projections.append(projection)

        np.savez_compressed(projection_path, proj0=projections[0], proj1=projections[1], proj2=projections[2],
                            accession_number=accession_number, study_date=study_date)

        tqdm_iter.write("Done.")
        if use_nift:
            ret[ID] = {"covid_borders": covid_borders_path, "lung_borders": lung_borders_path, "lung": lung_path, "covid": covid_path,
                       "inpt_data": inpt, "lung_data": lung, "covid_data": covid}
        else:
            ret[ID] = {"covid_borders": covid_borders, "lung_borders": lung_borders, "lung_path": lung_path, "covid_path": covid_path,
                       "projection_path": projection_path, "axial_projection": projections[-1]}
        ret[ID].update(dict_seg_report)
    return ret


if __name__ == "__main__":
    '''
    Unit test
    '''
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--desc", default="coedet",
                        help="What weight to use. Default is best weight.")
    parser.add_argument("-m", "--models", default="models",
                        help="Where checkpoints are.")
    parser.add_argument('-i', '--input', type=str, required=True,
                        help=("Input .nii/.nii.gz file or folder with them (recursive search)."
                              "Supply 'validation' to use validation dataloader."))
    parser.add_argument('-o', '--output', type=str, required=True,
                        help="Folder where to store output.")
    parser.add_argument('-c', '--cpus', type=int, default=mp.cpu_count(),
                        help="Number of cores used. A higher number consumes a lot more RAM. Defaults to available cores.")
    parser.add_argument('--compress', action="store_true",
                        help="Output masks are compressed.")
    parser.add_argument('--cpu', action="store_true",
                        help="Don't use a GPU.")
    args = parser.parse_args()
    print(vars(args))

    prepare_output(args.output)

    device = "cpu" if args.cpu else "cuda:0"

    # Load model
    model = load_model_by_desc(args.desc, args.models, device=device)

    validation = args.input == "validation"

    normalization = Compose([NormalizeCT(minv=-1024, maxv=600),
                             FixWrongTargets(),
                             CovidTargetProcessing(),
                             ToTensor(volumetric=True)])

    # Check input
    if validation:
        dataloader = CTCovid19("data", "validation", transform=normalization,
                               return_meta_data=True).get_dataloader(batch_size=1, num_workers=args.cpus, shuffle=False)
        P_dices, L_dices = [], []
    else:
        if os.path.isfile(args.input):
            print("Detected file input.")
            ct_paths = [args.input]
        elif os.path.isdir(args.input):
            print("Detected folder input. Looking for .nii")
            ct_paths = sorted(list(glob.iglob(os.path.join(args.input, "**", "*.nii*"), recursive=True)))
            if len(ct_paths) == 0:
                print(f"Didn't find any .nii or .nii.gz files in folder {args.input}")
                quit()
        else:
            raise ValueError(f"Didn't find anything to process in {args.input}")

        print(f"Will process: {ct_paths}")

        dataloader = TestDataloaderLike(ct_paths, normalization, num_workers=args.cpus)

    P_dices = []
    L_dices = []

    try:
        orientation_information = ["Left", "Right"]
        predict_2d_stack(model, dataloader, validation, args.output, args.compress, DICEMetric(skip_ths=True),
                         P_dices, L_dices, device, orientation_information)

        if validation:
            P_dicem = round(np.array(P_dices).mean(), 2)
            L_dicem = round(np.array(L_dices).mean(), 2)
            print(f"Final P_dice mean: {P_dicem}\nFinal L_dice mean: {L_dicem}")
            with open(os.path.join(args.output, f"edet_validation_P_dice:{P_dicem}L_dice:{L_dicem}.txt"), 'w') as res_file:
                res_file.write(f"{P_dices}\n{L_dices}")

    except KeyboardInterrupt:
        pass
    except Exception:
        traceback.print_exc()
