# Requirements
Developed in Ubuntu 18.04 and Python 3.6.9


# Environment
Used python libraries are listed in requirements.txt, install with

    pip install -r requirements.txt

Recommended PyTorch version >= 1.5.1 and PyTorch Lightning >= 0.8.5.

Additionally, DLPT needs to be installed with pip, wheel provided in the dlpt_installer folder.

    pip install dlpt_installer/DLPT-*-py3-none-any.whl
