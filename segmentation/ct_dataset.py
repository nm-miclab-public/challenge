import os
import glob
import h5py
import traceback
from math import inf
from argparse import ArgumentParser

import numpy as np
import nibabel as nib
from tqdm import tqdm
from matplotlib import pyplot as plt
from sklearn.model_selection import train_test_split
from torch.utils.data import Dataset, DataLoader

from transforms import desc2transform, ToTensor, Compose, NormalizeCT, FixWrongTargets, CovidTargetProcessing


class CTCovid19(Dataset):
    CLASSES = ["P", "L"]  # Pulmão, Lesão

    # Remove corrupted
    REMOVED_TRAIN_IDS = ["6ebb5f7f4c5029fc2f20f3b03856fc6cbea3a217bd126d35a0ca432aed497ad7",
                         "a9993ab43d708f23f29c0ed7653ec248275b5ac6a3a0e2d4861aa512b6a5b30b",
                         "81954c93d9afb5cb878be049536aedd46eee8474084f959d8028b66b0e70e7eb",
                         "d3d50055cabac7dab8652ca85b3557d2ffc5ffdfbf2fece7183681aacc310f80",
                         "df549563cb75471c3b55e238a9e9700144a6a2540d0436f5122ddb02304b1cf6"]

    def __init__(self, path, mode, transform=None, return_meta_data=False):
        assert mode in ["all", "train", "validation", "test"]
        assert os.path.isdir(path)

        train_subjects = sorted(glob.glob(os.path.join(path, "TREINO", "*")))
        test_subjects = sorted(glob.glob(os.path.join(path, "TEST", "*")))

        removed = 0
        to_remove = []
        for subject in train_subjects:
            for remove_id in CTCovid19.REMOVED_TRAIN_IDS:
                if remove_id in subject:
                    to_remove.append(subject)
        for tr in to_remove:
            train_subjects.remove(tr)
            removed += 1

        all_subjects = train_subjects + test_subjects

        assert removed == len(CTCovid19.REMOVED_TRAIN_IDS), "Didn't found all IDs to be removed."

        self.subjects = {}
        self.subjects["all"] = all_subjects
        self.subjects["train"], self.subjects["validation"] = train_test_split(train_subjects, train_size=0.8, test_size=0.2, shuffle=True,
                                                                               random_state=4321)
        self.subjects["test"] = test_subjects

        self.mode = mode
        self.transform = transform
        self.return_meta_data = return_meta_data
        print(f"\nInitialized {self.mode} CTCovid19 with {len(self.subjects[self.mode])} subjects.\nTransform: {self.transform}\n")

    def __len__(self):
        return len(self.subjects[self.mode])

    def __getitem__(self, i):
        subject_folder = self.subjects[self.mode][i]
        subject_ID = os.path.basename(subject_folder)

        meta_data = {}

        data = nib.load(os.path.join(subject_folder, subject_ID + ".nii"))
        meta_data["ID"] = subject_ID
        meta_data["affine"] = data.affine
        data = data.get_fdata(dtype=np.float32)
        meta_data["shape"] = np.array(data.shape)

        if self.mode == "test" or "test" in subject_folder.lower():
            mask = data
        else:
            mask = nib.load(os.path.join(subject_folder, subject_ID + "_mask.nii")).get_fdata(dtype=np.float32)

        if self.transform is not None:
            data, mask = self.transform(data, mask)

        ret = (data, mask)

        if self.return_meta_data:
            ret += (meta_data,)

        return ret

    def get_dataloader(self, batch_size, num_workers, shuffle):
        return DataLoader(self, batch_size=batch_size, num_workers=num_workers, shuffle=shuffle)


class CTCovid192D(CTCovid19):
    CLASSES = ["P", "L"]  # Pulmão, Lesão

    def __init__(self, path, mode, transform=None, h5path="data/axial.hdf5",):
        super().__init__(path, mode, transform, True)

        self.h5path = h5path
        self.slices = []
        self.transform = transform
        IDS = [os.path.basename(subject) for subject in self.subjects[mode]]
        with h5py.File(h5path, 'r') as h5file:
            keys = h5file.keys()
            for key in keys:
                for ID in IDS:
                    if ID in key:
                        self.slices.append(key)

    def __len__(self):
        return len(self.slices)

    def __getitem__(self, i):
        with h5py.File(self.h5path, 'r') as h5file:
            group = h5file[self.slices[i]]

            data = group["data"][:]
            mask = group["target"][:]
            meta_data = {"ID": group["ID"][()], "affine": group["affine"][:], "shape": group["shape"][:]}

        if self.transform is not None:
            data, mask = self.transform(data, mask)

        return data, mask, meta_data

    @staticmethod
    def generate_slices(axis=2):
        volumes = CTCovid19("data", "all", transform=Compose([NormalizeCT(minv=-1024, maxv=600),
                                                              FixWrongTargets(),
                                                              CovidTargetProcessing(),
                                                              ToTensor(volumetric=True)]),
                            return_meta_data=True).get_dataloader(batch_size=1, num_workers=4, shuffle=False)

        if axis == 2:
            with h5py.File(os.path.join("data", "axial.hdf5"), 'a') as h5write:
                for volume, mask, meta_data in tqdm(volumes, position=1, total=len(volumes)):
                    volume, mask = volume.squeeze().numpy(), mask.squeeze().numpy()

                    ID, affine, shape = meta_data["ID"][0], meta_data["affine"][0].numpy(), meta_data["shape"][0].numpy()

                    for i, n in enumerate(tqdm(range(volume.shape[axis]), position=0, total=volume.shape[axis])):
                        slic, mask_slic = volume[:, :, n], mask[:, :, :, n]
                        if mask_slic.sum() > 0:
                            assert slic.shape == (512, 512) and mask_slic.shape == (2, 512, 512)
                            group = h5write.create_group(ID + '_' + str(i))
                            group.create_dataset("data", data=slic, compression="lzf")
                            group.create_dataset("target", data=mask_slic, compression="lzf")
                            group.create_dataset("ID", data=ID)
                            group.create_dataset("affine", data=affine)
                            group.create_dataset("shape", data=shape)
        else:
            raise NotImplementedError


def display(npct, npm, ID=''):
    print(f"Pulmão volume: {npm[0].sum()}")
    print(f"Lesão volume: {npm[1].sum()}")
    print(npct.shape, npm.shape)
    print(npct.dtype, npm.dtype)
    print(npct.max(), npm.max())

    imgs = []

    n = npct.shape[-1]//2
    imgs.append(npct[:, :, n])
    if len(npm.shape) == 4:
        imgs.append(npm[0, :, :, n])
        imgs.append(npm[1, :, :, n])
    else:
        imgs.append(npm[:, :, n])

    plt.figure(figsize=(12, 6))
    plt.title(f"{ID}: CT - Pulmão - Lesão")
    plt.imshow(np.hstack(imgs), cmap="gray")
    plt.show()


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('-l', '--len', type=int, default=inf)
    parser.add_argument('-m', '--mode', type=str, default="all")
    parser.add_argument('--desc', type=str, default=None)
    parser.add_argument('-c', '--cpus', type=int, default=0)
    parser.add_argument('-d', '--data', type=str, default="data")
    parser.add_argument('--tt', action="store_true")
    parser.add_argument('--display', action="store_true")
    parser.add_argument('--retmet', action="store_true")
    parser.add_argument('--generate_slices', action="store_true")
    args = parser.parse_args()

    if args.generate_slices:
        CTCovid192D.generate_slices()
        quit()

    print(vars(args))

    if args.desc is None:
        train_transform, test_transform = desc2transform(None)
    else:
        train_transform, test_transform = desc2transform(args.desc)

    dataset = CTCovid19(args.data, args.mode, transform=test_transform if args.tt else train_transform, return_meta_data=args.retmet)

    print(f"Number of images {len(dataset)}.")
    ctcovid19 = dataset.get_dataloader(batch_size=1, num_workers=args.cpus, shuffle=False)

    errors = []
    shapes = []
    maxes = []
    means = []
    mins = []
    weird_labels = []
    i = 0
    try:
        tqdm_iter = tqdm(ctcovid19)
        for batch in tqdm_iter:
            if args.retmet:
                ct, mask, meta_data = batch
                ID = meta_data["ID"][0]
            else:
                ID = ''
                ct, mask = batch

            tqdm_iter.write(f"{ct.shape}, {mask.shape}")

            if mask.max() >= 3 and args.retmet:
                tqdm_iter.write(f"Found an weird ID! {ID}")
                weird_labels.append(ID)

            if args.display:
                key = display(ct.squeeze().numpy(), mask.squeeze().numpy(), ID)
                if key == 27:
                    break

            shapes.append(ct.shape)
            maxes.append(ct.max())
            means.append(ct.mean())
            mins.append(ct.min())
            i += 1
            if i >= args.len:
                tqdm_iter.write("Limit achieved.")
                break
    except KeyboardInterrupt:
        tqdm_iter.write("Graciously exiting... Please wait until workers shutdown. Close visualizations.")
    except Exception as e:
        tqdm_iter.write(f"Error! {e}")
        errors.append(traceback.format_exc())

    plt.figure(num="Maxes")
    plt.plot(maxes)
    plt.figure(num="Means")
    plt.plot(means)
    plt.figure(num="Mins")
    plt.plot(mins)
    print(f"Shapes: {shapes}")
    for error in errors:
        print(error)
    print(f"These had weird labels: {weird_labels}")
    plt.show()
