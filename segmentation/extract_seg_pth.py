'''
Method used for extracting .pth from .ckpt files from classification training.
'''
import argparse
import torch

from experiment_edet import CoEDETExperiment


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('checkpoint')
    parser.add_argument('output')
    args = parser.parse_args()

    ckpt = CoEDETExperiment.load_from_checkpoint(args.checkpoint)
    torch.save(ckpt.state_dict(), args.output)
