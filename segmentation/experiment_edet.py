EXPERIMENT_NAME = "CoEDET"

# Standard Library
import os
import logging
import argparse
from multiprocessing import cpu_count

# External Libraries
import numpy as np
import torch

# Pytorch Lightning
import pytorch_lightning as pl
from pytorch_lightning import Trainer
from pytorch_lightning.loggers import MLFlowLogger
from pytorch_lightning.callbacks import ModelCheckpoint

# DLPT
from DLPT.loss.dice import DICELoss
from DLPT.metrics.segmentation import DICEMetric
from DLPT.optimizers.radam import RAdam
from DLPT.utils.git import get_git_hash
from DLPT.utils.logging import init_logging
from DLPT.utils.reproducible import deterministic_run

# Local
from ct_dataset import CTCovid192D
from transforms import coedet_transform
from coedet import CoEDET


class CoEDETExperiment(pl.LightningModule):
    def __init__(self, hparams):
        super().__init__()
        self.hparams = hparams

        # Transforms
        self.transform, self.test_transform = coedet_transform(self.hparams.trans)
        self.hparams.transform_str, self.hparams.test_transform_str = str(self.transform), str(self.test_transform)

        # Loss and metrics
        self.loss, self.metric = DICELoss(volumetric=True, per_channel=True), CoUNet3D_metrics()
        self.loss_str, self.metric_str = str(self.loss), str(self.metric)

        # Model initialization
        self.model = CoEDET(self.hparams.head)

    def forward(self, x):
        return self.model(x)

    def training_step(self, batch, batch_idx):
        '''
        Training
        '''
        x, y, _ = batch
        y_hat = self.forward(x)
        loss = self.loss(y_hat, y)

        tensorboard_logs = {'loss': loss}

        return {'loss': loss, 'log': tensorboard_logs}

    def validation_step(self, batch, batch_idx):
        '''
        Validation step.
        '''
        x, y, _ = batch
        y_hat = self.forward(x)
        loss = self.loss(y_hat, y)
        metrics = self.metric(y_hat, y)
        metrics["loss"] = loss

        return metrics

    # -----------------------------------------------------------------------------------------------------------------#

    def training_epoch_end(self, outputs):
        '''
        Training
        '''
        name = "train_"

        avg_loss = torch.stack([x['loss'] for x in outputs]).mean()

        return {'log': {name + 'loss': avg_loss}}

    def validation_epoch_end(self, outputs):
        '''
        Validation
        '''
        name = 'val_'

        # Initialize metric dict
        metrics = {}
        for k in outputs[0].keys():
            metrics[name + k] = []

        # Fill metric dict
        for x in outputs:
            for k, v in x.items():
                metrics[name + k].append(v)

        # Get mean for every metric
        for k, v in metrics.items():
            if 'loss' in k:
                metrics[k] = torch.stack(v).mean()
            else:
                metrics[k] = np.array(v).mean()

        avg_loss = metrics[name + "loss"]
        tqdm_dict = {name + "loss": avg_loss,
                     name + "P_dice": metrics[name + "P_dice"],
                     name + "L_dice": metrics[name + "L_dice"]}

        return {name + 'loss': avg_loss, 'log': metrics, 'progress_bar': tqdm_dict,
                name + "P_dice": metrics[name + "P_dice"],
                name + "L_dice": metrics[name + "L_dice"]
                }

    # -----------------------------------------------------------------------------------------------------------------#

    def configure_optimizers(self):
        opt = RAdam(self.model.parameters(), lr=self.hparams.lr, weight_decay=self.hparams.wd)

        return [opt], [torch.optim.lr_scheduler.ExponentialLR(opt, gamma=0.985)]

    def setup(self, stage):
        '''
        For multigpu training support
        '''
        self.train_dataset = CTCovid192D("data", "train", transform=self.transform, h5path=self.hparams.h5)
        self.validation_dataset = CTCovid192D("data", "validation", transform=self.test_transform, h5path=self.hparams.h5)

    def train_dataloader(self):
        # Backwards compatible
        dataset = getattr(self, "train_dataset", CTCovid192D("data", "train", transform=self.transform, h5path=self.hparams.h5))
        return dataset.get_dataloader(batch_size=self.hparams.bs, shuffle=True, num_workers=self.hparams.c)

    def val_dataloader(self):
        # Backwards compatible
        dataset = getattr(self, "validation_dataset", CTCovid192D("data", "validation", transform=self.test_transform,
                                                                  h5path=self.hparams.h5))
        return dataset.get_dataloader(batch_size=self.hparams.test_bs, shuffle=False, num_workers=self.hparams.c)


class CoUNet3D_metrics():
    def __init__(self):
        self.dice = DICEMetric(per_channel_metric=True)

    def __call__(self, preds, tgt):
        dices = self.dice(preds, tgt)
        report = {}

        for i, c in enumerate(CTCovid192D.CLASSES):
            report[f"{c}_dice"] = dices[i]

        return report


if __name__ == "__main__":
    model_folder = "models"
    log_folder = "logs"

    parser = argparse.ArgumentParser()
    parser.add_argument(dest="desc", type=str)

    parser.add_argument("-bs", type=int, default=12)
    parser.add_argument("-acum", type=int, default=1)
    parser.add_argument("-precision", type=int, default=32)
    parser.add_argument("-debug", action="store_true")
    parser.add_argument("-lr", type=float, default=1e-3)
    parser.add_argument("-wd", type=float, default=1e-5)
    parser.add_argument("-test_bs", type=int, default=1)
    parser.add_argument("-max_epochs", type=int, default=300)
    parser.add_argument("-c", type=int, default=cpu_count())
    parser.add_argument("-rseed", type=int, default=4321)
    parser.add_argument("-forced_overfit", type=float, default=0)
    parser.add_argument("-dataset", type=str, default="CTCovid19")
    parser.add_argument("-gpus", type=str, default='1')
    parser.add_argument("-h5", type=str, default="data/axial.hdf5")
    parser.add_argument("-trans", type=str, default="baseline")
    parser.add_argument("-head", type=str, default=None)

    hyperparameters = parser.parse_args()

    init_logging(hyperparameters.debug)

    print("Experiment Hyperparameters:\n")
    print(vars(hyperparameters))

    # Multi GPU parse
    try:
        gpus = int(hyperparameters.gpus)
        if gpus > 1:
            ddp = "ddp"
        else:
            ddp = None
    except ValueError:
        print("Couldn't interpret GPU number from gpu argument, trying list.")
        gpus = eval(hyperparameters.gpus)
        assert isinstance(gpus, list)
        try:
            _ = [int(x) for x in gpus]
        except ValueError:
            print(f"Could not interpret gpu argument {hyperparameters.gpus}")
        if len(gpus) > 1:
            ddp = "ddp"
        else:
            ddp = None

    # # Initialize Trainer
    # Instantiate model
    model = CoEDETExperiment(hyperparameters)

    # Folder management
    experiment_name = EXPERIMENT_NAME
    os.makedirs(model_folder, exist_ok=True)
    ckpt_path = os.path.join(model_folder, "-{epoch}-{val_loss:.4f}-{val_P_dice:.4f}-{val_L_dice:.4f}")

    if hyperparameters.debug:
        logging.warning("DEBUG MODE, DISABLING CALLBACKS")
        checkpoint_callback = False
        logger = False
    else:
        # Callback initialization
        checkpoint_callback = ModelCheckpoint(prefix=experiment_name + '_' + hyperparameters.desc, filepath=ckpt_path, monitor="val_loss",
                                              mode="min")
        logger = MLFlowLogger(experiment_name=experiment_name, tracking_uri="file:" + log_folder,
                              tags={"desc": hyperparameters.desc, "commit": get_git_hash()})

    # PL Trainer initialization
    trainer = Trainer(gpus=gpus, precision=hyperparameters.precision, checkpoint_callback=checkpoint_callback,
                      log_gpu_memory=True, early_stop_callback=False, logger=logger, max_epochs=hyperparameters.max_epochs,
                      fast_dev_run=hyperparameters.debug, progress_bar_refresh_rate=1,
                      overfit_batches=hyperparameters.forced_overfit, accumulate_grad_batches=hyperparameters.acum,
                      distributed_backend=ddp, deterministic=True)

    # # Training Loop
    deterministic_run()
    trainer.fit(model)
